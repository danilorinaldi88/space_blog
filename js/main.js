  
        
        document.addEventListener('scroll', ()=>{
            let bg = document.querySelector('.header')
            
            let wrapperMenu = document.querySelector('.system')
            wrapperMenu.style.transform = `rotateZ(${window.pageYOffset / 4}deg)`
            
            bg.style.backgroundSize = `100%, 4px 4px, ${100 - window.pageYOffset / 10}%`
        });
        
        document.querySelector('.wrapperMenu').addEventListener('click', ()=>{
            document.querySelector('.leftSide').classList.toggle('activeLeft')
            document.querySelector('.rightSide').classList.toggle('activeRight')
        })
        